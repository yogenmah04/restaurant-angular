import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BlogApiService } from '../../service/blog-api.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  constructor(private blogApiService:BlogApiService) { }

  ngOnInit() {
    this.blogApiService.get().subscribe((res:HttpResponse<any>)=>{
      console.log('data');
      console.log(res.body);

    });
  }

}
