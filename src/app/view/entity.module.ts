import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BannerModule } from './banner/banner.module';
import { HomeModule } from './home/home.module';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
    // prettier-ignore
    imports: [
        BannerModule,
        HomeModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [FooterComponent, HeaderComponent],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EntityModule {}
