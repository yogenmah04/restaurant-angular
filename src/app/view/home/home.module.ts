import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule
  ],
  entryComponents:[HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class HomeModule { }
