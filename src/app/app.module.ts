import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './view/home/home.component';
import { BannerComponent } from './view/banner/banner.component';
import { BlogComponent } from './view/blog/blog.component';
import { ContactusComponent } from './view/contactus/contactus.component';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';

import { MenuComponent } from './view/menu/menu.component';
import { TableComponent } from './view/table/table.component';
import { HeaderComponent } from './view/header/header.component';
import { FooterComponent } from './view/footer/footer.component';
import { EntityModule } from './view/entity.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    MenuComponent,
    ContactusComponent,
    BlogComponent,
    BannerComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpModule,
    HttpClientModule
  ],
  providers: [],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  entryComponents:[AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
