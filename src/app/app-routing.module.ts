import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './view/home/home.component';
import { MenuComponent } from './view/menu/menu.component';
import { TableComponent } from './view/table/table.component';
import { BannerComponent } from './view/banner/banner.component';
import { BlogComponent } from './view/blog/blog.component';
import { ContactusComponent } from './view/contactus/contactus.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Home'
    },
  },
  {
    path: 'menu',
    component: MenuComponent,
    data: {
      title: 'Home'
    },
  },
  {
    path: 'table',
    component: TableComponent,
    data: {
      title: 'Home'
    },
  },
  {
    path: 'blog',
    component: BlogComponent,
    data: {
      title: 'Home'
    },
  },
  {
    path: 'banner',
    component: BannerComponent,
    data: {
      title: 'Home'
    },
  },
  {
    path: 'contactus',
    component: ContactusComponent,
    data: {
      title: 'Home'
    },
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {



}
