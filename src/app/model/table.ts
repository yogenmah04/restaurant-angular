export const enum Status {
    INACTIVE = 'INACTIVE',
    ACTIVE = 'ACTIVE',
    DELETED = 'DELETED'
}
export interface ITable {
    id?: number;
    name?: string;
    status?: Status;
    seats?: string;
  }

export class Table implements ITable {
    constructor(
        public id?: number,
        public name?: string,
        public status?: Status,
        public seats?: string,
          ) {}
}
