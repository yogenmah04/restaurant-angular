export const enum Status {
    INACTIVE = 'INACTIVE',
    ACTIVE = 'ACTIVE',
    DELETED = 'DELETED'
}
export interface IContactUs {
    id?: number;
    name?: string;
    email?: string;
    contact?: string;
    message?: string;
    status?: Status;

  }

export class ContactUs implements IContactUs {
    constructor(
        public id?: number,
        public name?: string,
        public email?: string,
        public contact?: string,
        public message?: string,
        public status?: Status,
        ) {}
}
