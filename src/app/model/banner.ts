export const enum Status {
    INACTIVE = 'INACTIVE',
    ACTIVE = 'ACTIVE',
    DELETED = 'DELETED'
}
export interface IBanner {
    id?: number;
    title?: string;
    status?: Status;
    image?: any;
  }

export class Banner implements IBanner {
    constructor(
        public id?: number,
        public title?: string,
        public status?: Status,
        public image?: any,
        ) {}
}
