import { ICategory} from './category-id';
export const enum Status {
    INACTIVE = 'INACTIVE',
    ACTIVE = 'ACTIVE',
    DELETED = 'DELETED'
}
export interface IMenuList {
    id?: number;
    title?: string;
    price?: number;
    category?: ICategory;
    description?: string;
    status?: Status;
    image?: any;
    type?: string;
  }

export class MenuList implements IMenuList {
    constructor(
        public id?: number,
        public title?: string,
        public status?: Status,
        public image?: any,
        public price?: number,
        public category?: ICategory,
        public description?: string,
        public type?: string,
        ) {}
}
