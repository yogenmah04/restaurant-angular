export interface IBlogCategory {
    id?: number;
    title?: string;
}
export class IBlogCategory implements IBlogCategory {
    constructor(
        public id?: number,
        public title?: string,
      ) {}
}
