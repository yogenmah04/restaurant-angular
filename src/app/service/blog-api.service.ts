import { Observable , Subject} from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class BlogApiService {
    private resourceUrl = SERVER_API_URL ;
  constructor(private http: HttpClient) { }



    get(userData?: any): Observable<HttpResponse<any[]>> {
        return this.http.post<any[]>(this.resourceUrl+ '/api/blog', userData, { observe: 'response' });
    }
}
